"""
@ Author        : 马烈勇
@ CreateTime    : malieyong@163.com
@ ModifyTime    : 2021/3/2 下午10:57
@ Version       : Python 3.8
@ FileName      : fund_manager_info.py
@ Function      :  
@ Description   : 基金经理信息
"""


url = "http://fund.eastmoney.com/manager/#dt14;mcreturnjson;ftall;pn50;pi1;scabbname;stasc"


from selenium import webdriver


chrome_options = webdriver.ChromeOptions()
# 使用headless无界面浏览器模式
chrome_options.add_argument("--headless")  # 增加无界面选项
chrome_options.add_argument("--disable-gpu")  # 如果不加这个选项，有时定位会出现问题


# 启动浏览器，获取网页源代码
browser = webdriver.Chrome(chrome_options=chrome_options)
browser.maximize_window() # 浏览器全屏显示
browser.get(url=url)

tbody_list = browser.find_elements_by_xpath("//tbody")

tr_list = tbody_list[1].find_elements_by_xpath("//tr")

for i in tr_list:
    td_list = i.find_elements_by_xpath("//td")
    xuhao = td_list[0]
    xuhao = td_list[1].text
    xuhao = td_list[2].text

    xuhao = td_list[3].get_attribute(name="href")
    xuhao = td_list[3].get_attribute(name="title")
    if td_list[3].find_element_by_class_name(name="zhang"):



    xuhao = td_list[4]

browser.quit()
# browser.close()
