"""
@ Author        : 马烈勇
@ CreateTime    : malieyong@163.com
@ ModifyTime    : 2021/1/30 下午8:46
@ Version       : Python 3.8
@ FileName      : screening_info.py
@ Function      : 
@ Description   : 基金公司基本信息
"""


import requests
from bs4 import BeautifulSoup


url = "http://fund.eastmoney.com/company/default.html"

r = requests.get(url)
r.encoding = 'utf-8'
html = r.text

soup = BeautifulSoup(html, features="html.parser")
soup.prettify()  # 处理缩进
table = soup.find("table", id="gspmTbl")
tbody = table.find("tbody")
tr_list = tbody.find_all("tr")
import json

with open(r"../fund_data/company_info.json", mode="w", encoding='utf-8') as file:
    for tr in tr_list:
        company_info = {}
        td = tr.find_all("td")
        company_info["company_name"] = td[1].text            # 基金公司名称
        company_info["company_create_time"] = td[3].text     # 基金公司成立时间

        td_text = td[5].text
        td_1 = td_text.replace("\n", "")
        td_2 = td_1.strip()
        company_info["company_size"] = td_2[0:-8]           # 基金公司管理规模

        company_info["company_fund_number"] = td[6].text     # 全部基金数
        company_info["company_manager_number"] = td[7].text  # 全部经理数
        json.dump(company_info, file, ensure_ascii=False)
        file.write("\r\n")


